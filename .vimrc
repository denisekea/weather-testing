"Basic highlight
syntax on

"Disable annoying beeping:
set noerrorbells
set vb t_vb=

"To control the number of space characters that will be inserted when the tab key is pressed set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab

"Indention
set autoindent
set copyindent

"Display line numbers
set nu

"Spellcheck
set spell spelllang=en_us

"Character encoding
set encoding=utf-8
set fileencoding=utf-8

"Search
set smartcase
set incsearch

set undodir=~/.vim/undodir
set undofile

set colorcolumn=80
highlight ColorColumn ctermbg=0 guibg=lightgrey

"vim-plug - Vim plugin manager
call plug#begin('~/.vim/plugged')

"fuzzy finder - it's an interactive Unix filter for command-line
"that can be used with any list; files, command history, processes, etc.
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }

"Autocomplete, Flexible: configured like VSCode, extensions work like in
"VSCode,
Plug 'neoclide/coc.nvim', {'branch':'release'}

"Emmet - plug-in which provides support for expanding abbreviations similar to emmet
Plug 'mattn/emmet-vim'

"Retro color scheme
Plug 'gruvbox-community/gruvbox'

"Syntax highlighting
Plug 'sheerun/vim-polyglot'

"Asynchronous Lint Engine - hook into your linter’s configuration
"and show you errors in the gutter.
Plug 'w0rp/ale'

"The plug-in visualizes undo history and makes it easier to browse and switch
"between different undo branches. Not sure if it is that important?
Plug 'mbbill/undotree'

"NERDTree - it is a file system explorer for the Vim editor
Plug 'preservim/nerdtree'

call plug#end()

" vim-prettier
"let g:prettier#quickfix_enabled = 0
"let g:prettier#quickfix_auto_focus = 0
" prettier command for coc
command! -nargs=0 Prettier :CocCommand prettier.formatFile
" run prettier on save
"let g:prettier#autoformat = 0
"autocmd BufWritePre .js,.jsx,.mjs,.ts,.tsx,.css,.less,.scss,.json,.graphql,.md,.vue,.yaml,.html PrettierAsync

" coc configuration
let g:coc_global_extensions = [
\ 'coc-pairs',
\ 'coc-tsserver',
\ 'coc-eslint',
\ 'coc-prettier',
\ 'coc-json',
]

colorscheme gruvbox
set background=dark