# Weather app

## Get started

npm install

Run server with Parcel: npm start

## Run build

npm run build

## Run tests with Jest

npm test
