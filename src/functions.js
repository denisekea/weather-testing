const fetch = require("node-fetch"); // for jest test
const rainySVG = require("./assets/rainy.svg");
const sunnySVG = require("./assets/sunny.svg");
const cloudySVG = require("./assets/cloudy.svg");
const fewcloudsSVG = require("./assets/fewclouds.svg");
const snowySVG = require("./assets/snowy.svg");

const fetchWeatherData = async (cityName) => {
  const response = await fetch(
    `https://api.openweathermap.org/data/2.5/weather?q=${cityName}&units=metric&appid=90d6a96d786880735378860d2d4b2628`
  );
  if (response.ok) {
    const data = await response.json();
    return data;
  } else {
    return undefined;
  }
};

const changeVisuals = (weather, cloudiness) => {
  switch (weather) {
    case "Clear":
      weatherImg.src = sunnySVG;
      bgGradient.style.background =
        "linear-gradient(0deg, rgba(92,143,193,1) 0%, rgba(255,193,139,1) 70%)";
      break;
    case "Rain":
    case "Drizzle":
    case "Thunderstorm":
      weatherImg.src = rainySVG;
      bgGradient.style.background =
        "linear-gradient(0deg, rgba(187,209,224,1) 0%, rgba(47,58,83,1) 90%)";
      break;
    case "Clouds":
    case "Mist":
      if (cloudiness < 50) {
        weatherImg.src = fewcloudsSVG;
        bgGradient.style.background =
          "linear-gradient(180deg, rgba(184,233,255,1) 9%, rgba(148,204,172,1) 78%)";
      } else {
        weatherImg.src = cloudySVG;
        bgGradient.style.background =
          "linear-gradient(0deg, rgba(182,224,255,1) 0%, rgba(128,144,168,1) 90%)";
      }
      break;
    case "Snow":
      weatherImg.src = snowySVG;
      bgGradient.style.background =
        "linear-gradient(0deg, rgba(217,243,255,1) 0%, rgba(90,137,228,1) 78%)";
      break;
    default:
      weatherImg.src = fewcloudsSVG;
      bgGradient.style.background =
        "linear-gradient(180deg, rgba(184,233,255,1) 9%, rgba(148,204,172,1) 78%)";
  }
};

const displayWeather = (data) => {
  changeVisuals(data.weather[0].main, data.clouds.all);
  tempResult.textContent = data.main.temp.toFixed();
  cityDisplay.textContent = data.name;
  feelsLikeResult.textContent = data.main.feels_like.toFixed();
  windResult.textContent = data.wind.speed.toFixed(1);
  rainResult.textContent = (data.rain && data.rain["1h"].toFixed(1)) || 0;
  cloudsResult.textContent = data.clouds.all;
};

module.exports = {
  fetchWeatherData: fetchWeatherData,
  changeVisuals: changeVisuals,
  displayWeather: displayWeather,
};
