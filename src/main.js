const { fetchWeatherData, displayWeather } = require("./functions");

const citySelect = document.querySelector("#select-cityDropdown");

citySelect.addEventListener("change", async (event) => {
  const newData = await fetchWeatherData(event.target.value);
  newData && displayWeather(newData);
});

window.onload = async function () {
  const data = await fetchWeatherData(citySelect.value);
  data && displayWeather(data);
};
