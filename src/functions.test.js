const { fetchWeatherData } = require("./functions");

test("receive data with city name", async () => {
  expect(await fetchWeatherData("Copenhagen")).toBeDefined();
  expect(await fetchWeatherData("sdfsdf")).toBeUndefined();
});
